const next = document.getElementsByClassName("left")[0];
const prev = document.getElementsByClassName("right")[0];

let n = 1;

next.addEventListener("click", () => {
  fetch(`http://localhost:8080/pagination?page=${n + 1}`)
    .then(res => {
      return res.json();
    })
    .then(data => {
      const { allImages, pagination } = data;
      if (!allImages) {
        return;
      }
      console.log(n);

      n++;
      const imgServer = document.getElementsByClassName("img_form_server")[0];
      imgServer.src = `${allImages}`;
    })
    .catch(err => {
      console.log(err);
    });
});

prev.addEventListener("click", () => {
  fetch(`http://localhost:8080/pagination?page=${n - 1}`)
    .then(res => {
      return res.json();
    })
    .then(data => {
      const { allImages, pagination } = data;

      if (!allImages) {
        return;
      }
      n--;

      const imgServer = document.getElementsByClassName("img_form_server")[0];
      imgServer.src = `${allImages}`;
    })
    .catch(err => {
      console.log(err);
    });
});
