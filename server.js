const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const morgan = require("morgan");
const { img } = require("./index");
const port = 8080;
server = express();
const static = path.join(__dirname, "public");
const views = path.join(__dirname, "views");

server.use(
  bodyParser.urlencoded({
    extended: false
  })
);

const allImages = img.map(items => {
  return items;
});

server.get("/", (req, res) => {
  res.render("index", { allImages: allImages[0] });
});

server.get("/pagination", (req, res) => {
  const page = req.query.page;
  const limit = 1;
  const skip = (page - 1) * limit;
  console.log(page);

  res.json({
    allImages: allImages[skip],
    pagintaion: {
      total: allImages.length
    }
  });
});

server.use(bodyParser.json());
server.use(morgan("tiny"));
server.set("view engine", "ejs");
server.use(express.static(static));
server.set("views", views);

server.listen(port, () => console.log(`Server works on port:${port}`));
